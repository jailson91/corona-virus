import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import Home from '../views/Home.vue'
import ListaProcesso from '../views/ListaProcesso.vue'
import CadastrarUsuario from '../views/CadastrarUsuario.vue'
import Login from '../views/login.vue'
import ListaMovimentacao from '../views/listaMovimentacao.vue'
import cadastrarSetor from '../views/cadastrarSetor.vue'
import CargaRemesa from '../views/CargaRemesa.vue'

Vue.use(VueRouter)

var router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [

    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        admin: true,
        super: true,
        usuario: true,
        
      }
    },

    {
      path: '/cargaRemesa',
      name: 'CargaRemesa',
      component: CargaRemesa,
      meta: {
        admin: true,
        super: true,
        usuario: true,
        
      }
    },


    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        libre: true
        
      }
    },
  
    {
      path: '/listaProcesso',
      name: 'ListaProcesso',
      component: ListaProcesso,
      meta: {
        admin: true,
        super: true,
        usuario: true,
        
      }
    },

    {
      path: '/listaMovimentacao',
      name: 'ListaMovimentacao',
      component: ListaMovimentacao,
      meta: {
        admin: true,
        super: true,
        usuario: true,
        
      }
    },
    {
      path: '/cadastrarUsuario',
      name: 'CadastrarUsuario',
      component: CadastrarUsuario,
      meta: {
        admin: true,
        super: true,
        
      }
    },
    {
      path: '/cadastrarSetor',
      name: 'cadastrarSetor',
      component: cadastrarSetor,
      meta: {
        admin: true,
        super: true,
      }
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    }

  ]
})

router.beforeEach ((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next();
  } else if (store.state.usuario && store.state.usuario.papel == 'admin') {
    if (to.matched.some(record => record.meta.admin)) {
      next();
    }
  } else if (store.state.usuario && store.state.usuario.papel == 'super') {
    if (to.matched.some(record => record.meta.super)) {
      next();
    }
  } else if (store.state.usuario && store.state.usuario.papel == 'usuario') {
    if (to.matched.some(record => record.meta.usuario)) {
      next();
    }
  } else {
    next({name: 'Login'});
  }
});

export default router
