import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import Vuelidate from 'vuelidate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue'
import { VueMaskDirective } from 'v-mask'
import VueMask from 'v-mask'
import VueTheMask from 'vue-the-mask'

Vue.use(VueAxios, axios)
Vue.use(Vuelidate)
Vue.use(Vuex);
Vue.use(VueMask);
Vue.directive('mask', VueMaskDirective);
Vue.use(BootstrapVue)
Vue.use(VueTheMask)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
axios.defaults.baseURL = 'http://10.102.16.166:3000/api';

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
